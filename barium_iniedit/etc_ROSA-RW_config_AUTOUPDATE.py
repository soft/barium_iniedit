def item_helper(config, key, i, del_button_clicked, refresh, Gtk, show_del, **kwargs):
  check_active = True
  var = config[key][i][0]
  val = config[key][i][1].strip('"').strip("'")
  combo_tuple = ('ask', 'auto', 'never')

  if var.startswith('#'):
    var = var.lstrip('#')
    check_active = False 
  _dict = {}
  _dict['box'] = Gtk.Box(expand=True, hexpand=True, spacing=10)
  _dict['ok_button'] = Gtk.Button(stock=Gtk.STOCK_OK, halign='end')
  _dict['label'] = Gtk.Label(label=var, xalign=0.0)
  _dict['label'].set_width_chars(30)
  _dict['flag'] = Gtk.CheckButton(halign='start')
  _dict['flag'].set_active(check_active)
  _dict['combo'] = Gtk.ComboBoxText()
  for a in combo_tuple:
    _dict['combo'].append_text(a)
  if val == combo_tuple[0]:
    _dict['combo'].set_active(0)
  else:
    _dict['combo'].set_active(1)
  for item in ('flag', 'label'):
    _dict['box'].pack_start(_dict[item], False, True, 0)
  for item in ('combo', 'ok_button'):
    _dict['box'].pack_start(_dict[item], True, True, 0)
  _dict['ok_button'].connect("clicked", OK_button_clicked, _dict, config, key, i,refresh )
  _dict['flag'].connect("toggled", OK_button_clicked, _dict, config, key, i,refresh )
  _dict['combo'].connect("changed", OK_button_clicked, _dict, config, key, i,refresh )
  if show_del:
    _dict['del_button'] = Gtk.Button(stock=Gtk.STOCK_DELETE, halign='end')
    _dict['box'].pack_start(_dict['del_button'], False, True, 0)
    _dict['del_button'].connect("clicked", del_button_clicked, key, i)
  return _dict

def OK_button_clicked (button, _dict, config, key, i,refresh ):
  text = _dict['combo'].get_active_text()
  if _dict['flag'].get_active():
    config[key][i] = config[key][i][0].lstrip('# ') + '=' + text
  else:
    config[key][i] = '# ' + config[key][i][0].lstrip('# ') + '=' + text
  refresh()
  
