def item_helper(config, key, i, del_button_clicked, refresh, Gtk, Gdk, show_del, _, **kwargs):
  check_active = True
  var = config[key][i][0]
  val = config[key][i][1]
  if var.startswith('#'):
    var = var.lstrip('#')
    check_active = False 
  _dict = {}
  _dict['box'] = Gtk.Box(expand=True, hexpand=True, spacing=10)
  _dict['ok_button'] = Gtk.Button(stock=Gtk.STOCK_OK, halign='end')
  _dict['label'] = Gtk.Label(label=var, xalign=0.0)
  _dict['label'].set_width_chars(30)
  _dict['entry_hash'] = Gtk.Entry(text=val, halign='fill', hexpand=True, expand=True)
  _dict['entry_hash'].set_editable(False);
  _dict['flag'] = Gtk.CheckButton(halign='start')
  _dict['flag'].set_active(check_active)
  _dict['entry'] = Gtk.Entry(placeholder_text='passwd', halign='fill', hexpand=True, expand=True)
  for item in ('flag', 'label', 'entry', 'entry_hash', 'ok_button'):
    _dict['box'].pack_start(_dict[item], False, True, 0)
  _dict['ok_button'].connect("clicked", OK_button_clicked, _dict, config, key, i,refresh, Gtk, Gdk, _ )
  _dict['flag'].connect("toggled", OK_button_clicked, _dict, config, key, i,refresh, Gtk, Gdk, _ )
  _dict['entry'].connect("activate", OK_button_clicked, _dict, config, key, i,refresh, Gtk, Gdk, _ ) 
  if show_del:  
    _dict['del_button'] = Gtk.Button(stock=Gtk.STOCK_DELETE, halign='end')
    _dict['box'].pack_start(_dict['del_button'], False, True, 0)
    _dict['del_button'].connect("clicked", del_button_clicked, key, i)
  return _dict

def OK_button_clicked (button, _dict, config, key, i,refresh, Gtk, Gdk, _ ):
  password = _dict['entry'].get_text().strip()
  if not password.startswith("'$6$"):
    res = check (password, _)
    if res == True:
      text = getHash(password)
    else:
      text = _("Not valid password: ") + res
      _dict['entry'].set_text(text)
      _dict['entry'].modify_fg(Gtk.StateFlags.NORMAL, Gdk.color_parse("red"))
      return
  else:
    text = password
  if _dict['flag'].get_active():
    config[key][i] = config[key][i][0].lstrip('# ') + '=' + text
  else:
    config[key][i] = '# ' + config[key][i][0].lstrip('# ') + '=' + text
  refresh()

def getHash(passwd):
  import random, string, crypt
  randomsalt = ''.join(random.sample(string.ascii_letters,8))
  return "'" + crypt.crypt(passwd, f'$6${randomsalt}$') + "'"

def check (text, _):
  if ' ' in text:
    return _('please no spaces')
  try:
    import pwquality
    try:
      pw_quality = pwquality.PWQSettings()
      pw_quality.read_config()
      pw_quality.check(text, None, None)
    except pwquality.PWQError as msg:
      return str(msg).split(' ', 1)[1].strip("()'")
  except:  
    if len(text) <= 5:
      return _('must be 6 or more characters')
  return True
    
