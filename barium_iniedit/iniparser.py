#!/bin/env python3
import importlib.util

def raw_conf(inifile):
  try:
    with open(inifile, 'r') as ini:
      lines = ini.readlines()
    return lines
  except:
    print(f'read {inifile} error')
    return False

def iniread(lines):
  '''
  read ini from var "lines", return dictionary where the keys are ini sections names
  if dict[key][n] is a str - single sring or comment
  if dict[key][n] is a list, dict[key][n][0] is var dict[key][n][1] is val
  config[key][0][1] == _is_active
  config[key][1][1] == _perms
  config[key][2][1] == _run_as special values for ini section headers
  '''
  conf = {}
  section = '_header'
  conf[section] = []
  comment_mark = '#'
  for line in lines:
    line = line.strip()
    # if line is empty, or line starts with 10 hashes it is a end of header part
    # in main part comments must start with '##'
    if not line or line.startswith('##########'):
      comment_mark = '##'
      conf[section].append(line)
      continue
    if line.startswith(comment_mark):
      conf[section].append(line)
      continue
    # if the line starts with '[' or '# [' in is a ini section header 
    if line.lstrip('# ').startswith('['):
      section = line.split(']')[0].strip('#[ ')
      conf[section] = []
      # append additional parameters for all ini headers except top
      if not section == '_header':
        _is_active = False if line.strip().startswith('#') else True
        perms = None
        run_as = None
        line = line.strip('#[] ')
        if '[' in line and ']' in line:
          perms = line[line.find(']')+1 : line.find('[')].strip()
          run_as = line[line.find('[')+1 : ]
        elif ']' in line and not '[' in line:
          perms = line[line.find(']')+1 :].strip()
          run_as = None
        conf[section].append(['_is_active', _is_active ])
        conf[section].append([ '_perms', perms ])
        conf[section].append([ '_run_as', run_as ])

      continue
    # set separator between var and val, for some sections it may be not '='
    separ = get_separator(section)
    comment = '#' if line.lstrip().startswith('#') else ''
    # if line was commented with spaces or without, remove hash simbols and spaces and set again as '# var'
    if separ in line and not \
	' ' in line.lstrip('# ').split(separ)[0] \
	and not line.strip('# ').startswith('+') \
	and not line.strip('# ').startswith('|') \
	and not line.strip('# ').startswith('-'):
        conf[section].append( [ comment + line.lstrip('# ').split(separ)[0],  line.lstrip('# ').split(separ, 1)[1] ] )
    else:
        conf[section].append(line)
  return conf

def iniwrite(config, inifile):
  '''
  write parameters from dict "config", to a temporary file "inifile"
  '''
  try:
    with open(inifile, 'w') as ini:
      for key in config:
        separ = get_separator(key)
        for item in config[key]:
          if (isinstance(item, str)):
            print(item, file=ini)
          elif item[0] == '_is_active':
            if item[1]:
              print(f'[{key}]', file=ini, end='', sep='')
            else:
              print(f'# [{key}]', file=ini, end='', sep='')
          elif item[0] == '_perms':
            if item[1] is not None:
              print(f'{item[1]} ', file=ini, end='')
          elif item[0] == '_run_as':
            if item[1] is not None:
              print(f'[{item[1]}]', file=ini)
            else:
              print('', file=ini)
          else:
            if config[key][0][1]:
              print(f'{item[0]}{separ}{item[1]}', file=ini)
            else:
              if item[0].strip().startswith('#'):
                print(f'{item[0]}{separ}{item[1]}', file=ini)
              else:
                print(f'# {item[0]}{separ}{item[1]}', file=ini)
  except:
    print(f'write {inifile} error')
    return False

def get_separator(section):
  # add sections where space is a var/val separator in this tuple
  # TODO read it from section module
  if section.lstrip('#') in ('/etc/ssh/sshd_config',):
    return ' '
  else:
    return '='
    
