#!/bin/bash
prefix=$(echo $LANG |cut -f1 -d _)
mkdir -p ./$prefix/LC_MESSAGES/
rm ./$prefix/LC_MESSAGES/iniedit.mo -f
msgfmt -v ./iniedit.po -o ./$prefix/LC_MESSAGES/iniedit.mo

