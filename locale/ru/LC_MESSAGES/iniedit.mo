��            )         �  	   �     �     �     �     �     �       e   (     �     �     �     �     �  "   �          !     4     J     _     s  0   �     �     �     �     �     �     �            B   2  �  u       0   4     e  0   n  .   �  '   �  4   �  �   +  1   �     $	  ;   3	  *   o	  8   �	  D   �	  )   
  0   B
  .   s
  '   �
  4   �
  7   �
  m   7     �     �  "   �  &   �  E     &   M  ;   t     �  �   �                                     
                                 	                                                                  Continue? ERROR: save ini file File  Hide "add" buttons Hide "delete" buttons Hide commented lines Hide disabled lines Iniedit will be closed so as not to create collisions.You can run it again after editing is complete. Not valid password:  Open Open ROSA.ini backup Open current ROSA.ini Open default ROSA.ini Save chenges  to currrent ini file Save settings in to: Show "add" buttons Show "delete" buttons Show commented lines Show disabled lines Show in a text editor The previous state of the ini file is backed up, View add cannot create cannot save must be 6 or more characters permissions denied please no spaces successfully saved! you can still load it into the editor window from the menu "Open". Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-03-06 15:51+0300
Last-Translator: Александр Михайлович <root@rosa>
Language-Team: Russian <gnu@d07.ru>
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Продолжить? ОШИБКА: не сохранен ini файл Файл Спрятать кнопки "Добавить" Спрятать кнопки "Удалить" Спрятать комментарии Спрятать отключенные строки Iniedit будет закрыт, чтобы не создавать коллизий. Вы можете открыть его снова после завершения редактирования. Пароль не прошел проверку:  Открыть Открыть резервную копию файла ini Открыть текущий файл ini Открыть файл ROSA.ini по умолчанию Сохранить изменения в текущий ini файл Сохранить настройки в: Показать кнопки "Добавить" Показать кнопки "Удалить" Показать комментарии Показать отключенные строки Открыть в текстовом редакторе Создана резервная копия ini файла с состоянием до сохранения Вид добавить Не удалось создать не удалось сохранить должно быть шесть или больше символов нет необходимых прав пожалуйста введите без пробелов сохранен удачно! Вы все еще можете загрузить его в окно редактора используя меню "Открыть" 